import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\Katalon Hacktiv8\\FINAL PROJECTS\\MOBILE\\app-debug.apk', true)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - CREATE A PROFILE (1)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - First Name (1)'), 'Citra', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Last Name (1)'), 'Sekar', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Country (1)'), 'Jakarta', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Username (1)'), 'citrasp', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText - Password (1)'), 'btDIBsWC4+X5P/BhQFnqDA==', 
    0)

Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText - Confirm Password (1)'), 'btDIBsWC4+X5P/BhQFnqDA==', 
    0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - Create Profile (1)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - LOGIN (4)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton (5)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.CheckedTextView - Make a Deposit (1)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - ADD ACCOUNT (1)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Account Name'), 'BCA', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Initial Balance (Optional) (2)'), '2000000', 
    0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - ADD (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton (5)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.CheckedTextView - Make a Deposit'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Enter Deposit Amount () (1)'), '500000', 0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - DEPOSIT (1)'), 0)

Mobile.closeApplication()

