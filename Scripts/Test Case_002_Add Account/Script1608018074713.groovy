import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\Katalon Hacktiv8\\FINAL PROJECTS\\MOBILE\\app-debug.apk', true)
Mobile.tap(findTestObject('Object Repository/android.widget.Button - CREATE A PROFILE'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - First Name'), 'Citra', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Last Name'), 'Sekar', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Country'), 'Jakarta', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Username'), 'citrasp', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText - Password'), 'btDIBsWC4+X5P/BhQFnqDA==',
	0)

Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText - Confirm Password'), 'btDIBsWC4+X5P/BhQFnqDA==',
	0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - Create Profile'), 0)


Mobile.tap(findTestObject('Object Repository/android.widget.Button - LOGIN'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton (1)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.CheckedTextView - Accounts'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageButton (2)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Account Name (1)'), 'BNI', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Initial Balance (Optional) (1)'), '3000000', 
    0)

Mobile.tap(findTestObject('Object Repository/android.widget.Button - ADD (1)'), 0)



Mobile.closeApplication()

